package com.easysolution.rabbitmqmanager;

import com.easysolution.rabbitmqmanager.configuration.AMQPConfig;
import com.easysolution.rabbitmqmanager.configuration.ApplicationConfig;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@EnableRabbit
@SpringBootApplication
public class RabbitmqManagerApplication extends SpringBootServletInitializer implements RabbitListenerConfigurer {

	@Autowired
	AMQPConfig amqpConfig;

	public static void main(String[] args) {
		SpringApplication.run(RabbitmqManagerApplication.class, args);
	}

	@Override
	public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
		registrar.setMessageHandlerMethodFactory(amqpConfig.messageHandlerMethodFactory());
	}

}
