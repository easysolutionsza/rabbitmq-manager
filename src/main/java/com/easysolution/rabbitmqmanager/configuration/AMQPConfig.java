package com.easysolution.rabbitmqmanager.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
public class AMQPConfig  {

    @Autowired
    private ApplicationConfig appconfig = new ApplicationConfig();

    @Bean
    TopicExchange exchange() {

        return new TopicExchange(appconfig.getExchangeName());
    }

    @Bean
    Queue queue1() {
        return new Queue(appconfig.getQueueName1(), false);
    }
    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(queue1()).to(exchange()).with(appconfig.getRoutingKey1());
    }

    @Bean
    Queue queue2() {
        return new Queue(appconfig.getQueueName2(), false);
    }
    @Bean
    public Binding binding2() {
        return BindingBuilder.bind(queue2()).to(exchange()).with(appconfig.getRoutingKey2());
    }


    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }
}
