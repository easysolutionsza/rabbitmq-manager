package com.easysolution.rabbitmqmanager.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfig {

    @Value("${app.exchange.name}")
    private String exchangeName;

    @Value("${app.queue.name1}")
    private String queueName1;
    @Value("${app.queue.name2}")
    private String queueName2;

    @Value("${app.routing.key1}")
    private String routingKey1;
    @Value("${app.routing.key2}")
    private String routingKey2;

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getQueueName1() {
        return queueName1;
    }

    public void setQueueName1(String queueName1) {
        this.queueName1 = queueName1;
    }

    public String getQueueName2() {
        return queueName2;
    }

    public void setQueueName2(String queueName2) {
        this.queueName2 = queueName2;
    }

    public String getRoutingKey1() {
        return routingKey1;
    }

    public void setRoutingKey1(String routingKey1) {
        this.routingKey1 = routingKey1;
    }

    public String getRoutingKey2() {
        return routingKey2;
    }

    public void setRoutingKey2(String routingKey2) {
        this.routingKey2 = routingKey2;
    }
}
