package com.easysolution.rabbitmqmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AssetDetails implements Serializable {



    private static final long serialVersionUID = 1L;
    @JsonProperty("assetID")
    private String assetID;
    @JsonProperty("assetName")
    private String assetName;
    @JsonProperty("assetPrice")
    private String assetPrice;
    @JsonProperty("currencyID")
    private String currencyID;
    @JsonProperty("assetLocation")
    private String assetLocation;


    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetPrice() {
        return assetPrice;
    }

    public void setAssetPrice(String assetPrice) {
        this.assetPrice = assetPrice;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getAssetLocation() {
        return assetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        this.assetLocation = assetLocation;
    }

}