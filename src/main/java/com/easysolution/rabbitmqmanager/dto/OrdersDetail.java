package com.easysolution.rabbitmqmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrdersDetail {

    private static final long serialVersionUID = 1L;

    @JsonProperty("orderID")
    private String orderID;
    @JsonProperty("orderName")
    private String orderName;
    @JsonProperty("orderPrice")
    private String orderPrice;
    @JsonProperty("currencyID")
    private String currencyID;
    @JsonProperty("quantity")
    private String quantity;


    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
