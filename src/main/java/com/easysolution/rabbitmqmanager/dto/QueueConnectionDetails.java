package com.easysolution.rabbitmqmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class QueueConnectionDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty("exchangeName")
    private String exchangeName;
    @JsonProperty("queueName")
    private String queueName;
    @JsonProperty("routingKey")
    private String routingKey;

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }


}
