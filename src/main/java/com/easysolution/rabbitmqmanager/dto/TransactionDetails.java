package com.easysolution.rabbitmqmanager.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TransactionDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("trnID")
    private String trnID;
    @JsonProperty("portfolioID")
    private String portfolioID;
    @JsonProperty("assetID")
    private String assetID;
    @JsonProperty("currencyID")
    private String currencyID;
    @JsonProperty("tradeDate")
    private String tradeDate;
    @JsonProperty("settleDate")
    private String settleDate;
    @JsonProperty("nominal")
    private String nominal;
    @JsonProperty("price")
    private String price;

    @JsonProperty("trnAmount")
    private String trnAmount;
    @JsonProperty("stampDuty")
    private String stampDuty;
    @JsonProperty("brokerage")
    private String brokerage;


    // Default constructor is needed to de-serialize JSON
    public TransactionDetails() {
    }

    public TransactionDetails(String trnID, String portfolioID, String assetID, String currencyID, String tradeDate, String settleDate, String nominal, String price, String trnAmount, String stampDuty, String brokerage) {
        this.trnID = trnID;
        this.portfolioID = portfolioID;
        this.assetID = assetID;
        this.currencyID = currencyID;
        this.tradeDate = tradeDate;
        this.settleDate = settleDate;
        this.nominal = nominal;
        this.price = price;
        this.trnAmount = trnAmount;
        this.stampDuty = stampDuty;
        this.brokerage = brokerage;
    }




    public String getTrnID() {
        return trnID;
    }

    public void setTrnID(String trnID) {
        this.trnID = trnID;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(String settleDate) {
        this.settleDate = settleDate;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(String trnAmount) {
        this.trnAmount = trnAmount;
    }

    public String getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(String stampDuty) {
        this.stampDuty = stampDuty;
    }

    public String getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(String brokerage) {
        this.brokerage = brokerage;
    }





    @Override
    public String toString() {
        return "TransactionDetails{" +
                "trnID='" + trnID + '\'' +
                ", portfolioID='" + portfolioID + '\'' +
                ", assetID='" + assetID + '\'' +
                ", currencyID='" + currencyID + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", settleDate='" + settleDate + '\'' +
                ", nominal='" + nominal + '\'' +
                ", price='" + price + '\'' +
                ", trnAmount='" + trnAmount + '\'' +
                ", stampDuty='" + stampDuty + '\'' +
                ", brokerage='" + brokerage + '\'' +
                '}';
    }



}
