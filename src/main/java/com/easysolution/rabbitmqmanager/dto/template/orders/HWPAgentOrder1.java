package com.easysolution.rabbitmqmanager.dto.template.orders;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HWPAgentOrder1 {


    private String orderID;
    private String orderName;
    private String orderPrice;

    private String routeKey;

    public String getRouteKey() {
        return routeKey;
    }

    public void setRouteKey(String routeKey) {
        this.routeKey = routeKey;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }


}
