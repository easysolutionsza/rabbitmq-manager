package com.easysolution.rabbitmqmanager.dto.template.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HWPAgentOne {
    private String trnID;
    private String portfolioID;
    private String assetID;
    private String currencyID;
    private String tradeDate;
    private String stampDuty;
    private String brokerage;

    public String getTrnID() {
        return trnID;
    }

    public void setTrnID(String trnID) {
        this.trnID = trnID;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(String stampDuty) {
        this.stampDuty = stampDuty;
    }

    public String getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(String brokerage) {
        this.brokerage = brokerage;
    }


}
