package com.easysolution.rabbitmqmanager.dto.template.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HWPAgentThree {

    private String trnID;
    private String portfolioID;
    private String assetID;
    private String currencyID;


    public String getTrnID() {
        return trnID;
    }

    public void setTrnID(String trnID) {
        this.trnID = trnID;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }






}
