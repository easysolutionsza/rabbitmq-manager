package com.easysolution.rabbitmqmanager.dto.template.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HWPAgentTwo {


    private String trnID;
    private String portfolioID;
    private String assetID;
    private String currencyID;
    private String tradeDate;
    private String settleDate;
    private String nominal;

    public String getTrnID() {
        return trnID;
    }

    public void setTrnID(String trnID) {
        this.trnID = trnID;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(String settleDate) {
        this.settleDate = settleDate;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

}
