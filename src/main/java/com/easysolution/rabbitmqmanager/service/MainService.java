package com.easysolution.rabbitmqmanager.service;

import com.easysolution.rabbitmqmanager.component.MessageSender;
import com.easysolution.rabbitmqmanager.configuration.AMQPConfig;
import com.easysolution.rabbitmqmanager.configuration.ApplicationConfig;
import com.easysolution.rabbitmqmanager.dto.*;
import com.easysolution.rabbitmqmanager.web.rest.ExchangeController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MainService {

    private static final Logger log = LoggerFactory.getLogger(ExchangeController.class);

    private RabbitTemplate rabbitTemplate;
    private ApplicationConfig applicationConfig;
    private AMQPConfig amqpConfig;
    private MessageSender messageSender;
    private static Map<String,List<String>> map = new HashMap<>();

    @Autowired
    public MainService(RabbitTemplate rabbitTemplate)
    {
        this.rabbitTemplate = rabbitTemplate;
    }

    public ApplicationConfig getApplicationConfig() {
        return applicationConfig;
    }

    @Autowired
    public void setApplicationConfig(ApplicationConfig applicationConfig, AMQPConfig amqpConfig) {
        this.applicationConfig = applicationConfig;
        this.amqpConfig = amqpConfig;
    }

    public MessageSender getMessageSender() {
        return messageSender;
    }

    @Autowired
    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setMap(List<QueueTemplate> queueTemplates)
    {
        //Map<String,Object> map = new HashMap<>();
        for (QueueTemplate queueTemplate: queueTemplates) {
            map.put(queueTemplate.getRoutingKey(),queueTemplate.getDataSet());
        }
    }

    public void sendTransactionMessage(TransactionDetails transactionDetails)
    {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> transactionMap = mapper.convertValue(transactionDetails, Map.class);

        for (Map.Entry<String, List<String>> entry : map.entrySet()) {

            String key = entry.getKey();
            List<String> dataset = entry.getValue();
            Map<String, Object> data = new HashMap<>();
            data.putAll(transactionMap);
            data.keySet().retainAll(dataset);

            try {
                messageSender.sendMessage(rabbitTemplate, applicationConfig.getExchangeName(), key, data);
                log.info("Massage send for key:" + key);
                log.info(new ResponseEntity<String>("REQUEST_IN_QUEUE", HttpStatus.OK).toString());

            } catch (Exception ex) {
                log.error("Exception occurred while sending message to the queue. Exception= {1}", ex);
                log.error(new ResponseEntity<>("MESSAGE_QUEUE_SEND_ERROR",
                        HttpStatus.INTERNAL_SERVER_ERROR).toString());
            }
        }

    }


    public ResponseEntity<?> sendOrderMessage(OrdersDetail ordersDetail)
    {
        /* Sending to Message Queue */
        try {
            messageSender.sendMessage(rabbitTemplate,getApplicationConfig().getExchangeName() , getApplicationConfig().getRoutingKey1(), ordersDetail);
            return new ResponseEntity<String>("REQUEST_IN_QUEUE", HttpStatus.OK);

        } catch (Exception ex) {
            log.error("Exception occurred while sending message to the queue. Exception= {1}", ex);
            return new ResponseEntity<>("MESSAGE_QUEUE_SEND_ERROR",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<?> sendAssetMessage(AssetDetails assetDetails)
    {
        /* Sending to Message Queue */
        try {
            messageSender.sendMessage(rabbitTemplate,getApplicationConfig().getExchangeName() , getApplicationConfig().getRoutingKey1(), assetDetails);
            return new ResponseEntity<String>("REQUEST_IN_QUEUE", HttpStatus.OK);

        } catch (Exception ex) {
            log.error("Exception occurred while sending message to the queue. Exception= {1}", ex);
            return new ResponseEntity<>("MESSAGE_QUEUE_SEND_ERROR",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public TransactionDetails pullMessage()
    {

        log.info("receiving the message ");
        Object message = rabbitTemplate.receiveAndConvert("my-app-queue");
        TransactionDetails transactionDetails=(TransactionDetails)message;
        System.out.println("Price--- "+transactionDetails.getPrice());

        if (message != null) {
            log.info("  received message [" + message.toString() + "] ");


        }

        return transactionDetails;
    }


  public String createQueueConnection(QueueConnectionDetails connectionsQ)
  {
      System.out.println("Inside Method  createQueueConnection" );

  try{
      ConnectionFactory factory = new ConnectionFactory();

      // "guest"/"guest" by default, limited to localhost connections
      factory.setUsername("guest");
      factory.setPassword("guest");
      factory.setVirtualHost("/");
      factory.setHost("localhost");
      factory.setPort(5672);


      Connection conn = factory.newConnection();
      Channel channel = conn.createChannel();

      channel.exchangeDeclare(connectionsQ.getExchangeName(), "topic", true);

      channel.queueDeclare(connectionsQ.getQueueName(), true, false, false, null);

      String queueName = connectionsQ.getQueueName();
       System.out.println("Qeueu nameeeee"+queueName);
      channel.queueBind(queueName, connectionsQ.getExchangeName(), connectionsQ.getRoutingKey());







      channel.close();
      conn.close();

      }
      catch (Exception e)
      {

      }

      //TopicExchange exchange= new TopicExchange(connectionsQ.getExchangeName());
      //Queue queue= new Queue(connectionsQ.getQueueName());
      //BindingBuilder.bind(queue).to(exchange).with(connectionsQ.getRoutingKey());
     // amqpConfig.binding(connectionsQ.getQueueName(),connectionsQ.getExchangeName(),connectionsQ.getRoutingKey());
      /*
      try {
          ConnectionFactory factory = new ConnectionFactory();
          factory.setHost("localhost");
          factory.setPort(15672);
          Connection connection = factory.newConnection();
          Channel channel = connection.createChannel();

          channel.basicPublish("", "products_queue", null, new String().getBytes());
      }
      catch (Exception ex)
      {

      } */


      return "Success";
  }
}
