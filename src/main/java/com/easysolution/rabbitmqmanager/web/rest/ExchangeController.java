package com.easysolution.rabbitmqmanager.web.rest;

import com.easysolution.rabbitmqmanager.dto.*;
import com.easysolution.rabbitmqmanager.service.MainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class ExchangeController {

    private static final Logger log = LoggerFactory.getLogger(ExchangeController.class);

    private MainService mainService;
    @Autowired
    public ExchangeController(MainService mainService)
    {
        this.mainService = mainService;
    }

    @PostMapping(path = "/produce/transaction", produces = MediaType.APPLICATION_JSON_VALUE)
    public void sendTransactions(@RequestBody TransactionDetails transactionDetails) {
        mainService.sendTransactionMessage(transactionDetails);
    }

    @PostMapping(path = "/produce/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public void sendOrders(@RequestBody OrdersDetail ordersDetail) {
        mainService.sendOrderMessage(ordersDetail);
    }

    @PostMapping(path = "/produce/assets", produces = MediaType.APPLICATION_JSON_VALUE)
    public void sendAssets(@RequestBody AssetDetails assetDetails) {
        mainService.sendAssetMessage(assetDetails);
    }

    @PostMapping(path = "/produce/createqueue", produces = MediaType.APPLICATION_JSON_VALUE)
    public void createQueue(@RequestBody QueueConnectionDetails qConnDetails) {

        mainService.createQueueConnection(qConnDetails);
    }

    @PostMapping(path = "/config/map", produces = MediaType.APPLICATION_JSON_VALUE)
    public void setTemplate(@RequestBody List<QueueTemplate> queueTemplate) {
        mainService.setMap(queueTemplate);
    }

    @GetMapping (path = "/consume/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionDetails> reciveMessage() {

        TransactionDetails transactionDetails= mainService.pullMessage();
        return new ResponseEntity<TransactionDetails>(transactionDetails, HttpStatus.OK);

    }

}
